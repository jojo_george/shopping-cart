var app = angular.module('ShopApp', []);

app.controller('ShopController', function($scope, Cartfactory, $http) {

    var url = 'http://jsonp.afeld.me/?url=https://api.myjson.com/bins/19ynm&callback';

    //polfill for Array.prototype.find for Explorer
    if (!Array.prototype.find) {
      Array.prototype.find = function(predicate) {
        if (this === null) {
          throw new TypeError('Array.prototype.find called on null or undefined');
        }
        if (typeof predicate !== 'function') {
          throw new TypeError('predicate must be a function');
        }
        var list = Object(this);
        var length = list.length >>> 0;
        var thisArg = arguments[1];
        var value;

        for (var i = 0; i < length; i++) {
          value = list[i];
          if (predicate.call(thisArg, value, i, list)) {
            return value;
          }
        }
        return undefined;
      };
    }

    $scope.$watch('cartData', function(newVal, oldVal) {
        var subtotal = 0;
        if (newVal === oldVal) {
            return false;
        }

        newVal.forEach(function(item) {
            subtotal += item.p_quantity * item.p_price;
        });

        $scope.subtotal = subtotal;
        $scope.estotal = subtotal;
    }, true);
    //function to apply coupon code
    $scope.applyCoupon = function($event) {
        var promoele = $event.currentTarget.parentNode.previousSibling.previousSibling;
        var code = promoele.value;
        if (code == '' || undefined) {
            return;
        }

        $(".promocode strong").html("PROMOTION CODE <span class='text-uppercase big-span'>" + code + "</span> APPLIED");

        $(".promocode")[0].childNodes[3].childNodes[2].nodeValue = "7.00";
        $scope.coupon_added = 7.00;
        $scope.estotal = $scope.estotal - $scope.coupon_added;
        promoele.value = '';
    };

    // function to set item for edit
    $scope.edit = function(product_id) {

        $scope.selectedItem = $scope.cartData.find(function(item) {
            return item.p_id === product_id;
        });
        $scope.isActive = getObjArrIndex($scope.selectedItem.p_available_options.colors, 'name', $scope.selectedItem.p_selected_color.name);
    };
    // function to update cart
    $scope.editProduct = function(p_id) {

        if ($scope.size) {
            $scope.selectedItem.p_selected_size = $scope.selectedItem.p_available_options.sizes.find(function(item) {

                return item.code === $scope.size;
            });
        }

        $scope.selectedItem.p_quantity = $scope.qty || $scope.selectedItem.p_quantity;
        $scope.selectedItem.p_selected_color = $scope.selectedItem.p_available_options.colors[$scope.isActive] || $scope.selectedItem.p_selected_color;

        var index = getObjArrIndex($scope.cartData, 'p_id', p_id);
        $scope.cartData.splice(index, 1, $scope.selectedItem);

        $('#editModal').modal('hide');
    };

    // Changing color buttons class
    $scope.activeClr = function(index) {

        $scope.isActive = index;
    }

    //function find index of object array
    function getObjArrIndex(arr, prop, key) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][prop] === key) {
                break;
            }
        }
        return i;
    }
    // function to delete from cart
    $scope.deleteProduct = function(p_id) {

        var index = getObjArrIndex($scope.cartData, 'p_id', p_id);
        $scope.cartData.splice(index, 1);
    };

    // function to update size
    $scope.passSize = function(size,event) {

        var $target = $(event.currentTarget);
        var $elem = $target.closest('.btn-group')
            .find('[data-bind="label"]');


        $elem.text($target.text())
            .end()
            .children('.dropdown-toggle').dropdown('toggle');

        $scope.size = size;

    };
    // function to update quantity

    $scope.passQty = function(qty) {
        $scope.qty = qty;
    };

    //Getting cartdata using factory call
    Cartfactory.get(url).success(function(response) {
            $scope.cartData = response.productsInCart;
        })
        .error(function(error) {
            console.log(error);
        });
});


// factory to get cart data from source url
app.factory('Cartfactory', function($http) {
    return {
        get: function(url) {
            return $http.get(url);
        }
    }

});

// filter to make text Camelcase
app.filter('titleCase', function() {
    return function(input) {
        input = input || '';
        return input.replace(/\w\S*/g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    };
});
